// React
// React
import React from 'react';

// Components
import SearchBar from './SearchBar';
import VideoList from './VideoList';
import VideoDetail from './VideoDetail';

// API call
import youtube from './../apis/youtube';

class App extends React.Component {

    state = { videos: [], selectedVideo: null };

    componentDidMount() {
        this.onTermSubmit('ReactJS');
    }

    onTermSubmit = async term => {
        const response = await youtube.get('/search', {
            params: {
                q: term
            }
        });

        this.setState({
            videos: response.data.items,
            selectedVideo: response.data.items[0],
        });
    };

    onVideoSelected = video => {
        this.setState({ selectedVideo: video });
    }

    render() {
        return(
            <div className="ui container" style={{ marginTop: "1em" }}>
                <SearchBar
                    onFormSubmit={this.onTermSubmit}
                />
                <div className="ui grid">
                    <div className="ui row">
                        <div className="eleven wide column">
                            <VideoDetail 
                                video={this.state.selectedVideo}
                            />
                        </div>
                        <div className="five wide column">
                            <VideoList
                                videos={this.state.videos}
                                onVideoSelected={this.onVideoSelected}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
