// Axios
import axios from 'axios';

// Constants
const KEY = 'AIzaSyC7CU-QBXYTeHOrzEZUEzIkte2Vf7-JzSg';

export default axios.create({
    baseURL: 'https://www.googleapis.com/youtube/v3',
    params: {
        part: 'snippet',
        type: 'video',
        maxResults: 5,
        key: KEY
    }
});
